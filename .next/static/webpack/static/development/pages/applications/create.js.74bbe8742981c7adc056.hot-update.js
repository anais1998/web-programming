webpackHotUpdate("static/development/pages/applications/create.js",{

/***/ "./components/ApplicationCreationForm.jsx":
/*!************************************************!*\
  !*** ./components/ApplicationCreationForm.jsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/anaislacombe/Documents/Web Programming/TP3-4/components/ApplicationCreationForm.jsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var _onSubmit = function onSubmit(company) {
  console.log("submiting");
  var token = window.sessionStorage.getItem("auth-token");

  if (!token) {
    throw new Error("Not logged in");
  }

  fetch("http://localhost:3000/applications/create", {
    method: "POST",
    headers: {
      Authorization: "Basic " + token,
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      company: company
    })
  }).then(function (res) {
    return res.json();
  }).then(console.log) // do not forget to catch errors
  ["catch"](console.log);
};

var ApplicationCreationForm = function ApplicationCreationForm() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      company = _useState[0],
      setCompany = _useState[1];

  var onCompanyChange = function onCompanyChange(evt) {
    setCompany(evt.target.value);
  };

  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, __jsx("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "Submit an internship application"), __jsx("form", {
    onSubmit: function onSubmit() {
      return _onSubmit(company);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, __jsx("label", {
    htmlFor: "company",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, "Company"), __jsx("input", {
    onChange: onCompanyChange,
    name: "company",
    value: company,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }), __jsx("button", {
    type: "submit",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, "Add application")));
};

/* harmony default export */ __webpack_exports__["default"] = (ApplicationCreationForm);

/***/ }),

/***/ "./pages/applications/create.js":
/*!**************************************!*\
  !*** ./pages/applications/create.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_ApplicationCreationForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/ApplicationCreationForm */ "./components/ApplicationCreationForm.jsx");
var _jsxFileName = "/home/anaislacombe/Documents/Web Programming/TP3-4/pages/applications/create.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


function ApplicationsCreate() {
  return __jsx(_components_ApplicationCreationForm__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  });
}

/* harmony default export */ __webpack_exports__["default"] = (ApplicationsCreate);

/***/ })

})
//# sourceMappingURL=create.js.74bbe8742981c7adc056.hot-update.js.map