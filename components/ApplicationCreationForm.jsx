import React, { useState } from "react";


const onSubmit = (company) => {
    console.log("submiting")
    const token = window.sessionStorage.getItem("auth-token")
    if (!token) {
        throw new Error("Not logged in")
    }
    fetch("http://localhost:3000/applications/create", {
        method: "POST",
        headers: {
            Authorization: "Basic " + token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            company
        })
    })
        .then(res => res.json())
        .then(console.log)
        // do not forget to catch errors
        .catch(console.log)
}

const ApplicationCreationForm = () => {
    const [company, setCompany] = useState("")
    const onCompanyChange = (evt) => { setCompany(evt.target.value) }
    return (
        <div>
            <h2>Submit an internship application</h2>
            <form onSubmit={() => onSubmit(company)}>
                <label htmlFor="company">Company</label>
                <input onChange={onCompanyChange} name="company" value={company} />
                <button type="submit">Add application</button>
            </form>
        </div>
    )

}
export default ApplicationCreationForm