/**
 * Example of data fetching with a container
 */

import React from "react"
import ApplicationsList from "./ApplicationsList"

class ApplicationsListContainer extends React.PureComponent {
    constructor() {
        super()
        this.state = {
            loading: true,
            applications: null,
            error: null
        }
    }
    // this function only runs in the browser
    // we can use client-side only API here,
    // even with SSR
    componentDidMount() {
        // reinit the state
        this.setState({
            loading: true,
            applications: null,
            error: null
        })
        const token = window.sessionStorage.getItem("auth-token")
        if (!token) {
            this.setState({
                loading: false,
                error: new Error("Not logged in")
            })
            return
        }
        fetch("http://localhost:3000/applications", {
            method: "GET",
            headers: {
                Authorization: "Basic " + token,
                "Content-Type": "application/json"
            },
        })
            .then(res => res.json())
            .then((applications) => {
                this.setState({
                    loading: false,
                    applications
                })
            })
            // do not forget to catch errors
            .catch((error) => {
                this.setState({
                    loading: false,
                    error,
                    applications: null
                })
            })
    }
    render() {
        const { loading, error, applications } = this.state
        if (loading) return "Loading"
        if (error) return "Error"
        return (
            <ApplicationsList applications={applications} />
        )
    }
}

export default ApplicationsListContainer